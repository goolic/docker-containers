# docker-containers
Various docker containers

Each container is now being hosted in a branch.  This is done to
alleviate Docker Auto-Builds from building all images when only
one of the container builds were changed.

See:
https://github.com/stevepacker/docker-containers/branches/all
