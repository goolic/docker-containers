# PHPSysInfo

## **DEPRECATED**

I am no longer maintaining this container image.

## **NO LONGER BEING MAINTAINED**

This creates a Docker container running [PHPSysInfo](https://phpsysinfo.github.io/phpsysinfo/) 
on [Alpine Linux](https://github.com/gliderlabs/docker-alpine).

### Example Usage:

    docker run --rm -it \
        -p 80:80 \
        stevepacker/phpsysinfo-alpine
